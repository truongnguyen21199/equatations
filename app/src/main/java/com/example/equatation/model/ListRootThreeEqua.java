package com.example.equatation.model;

public enum ListRootThreeEqua {

    HeVoNghiem("Hệ Vô Nghiệm"),
    HeVoSoNghiem("Hệ Vô số Nghiệm"),
    HeCoNghiem("Hệ có nghiệm");

    private String msg;
    ListRootThreeEqua(String msg)
    {
        this.msg=msg;
    }
    public String decription() {
        return this.msg;
    }
}
