package com.example.equatation.model;

import java.util.ArrayList;
import java.util.List;

public enum ListRootEquation {

        NghiemKep("phương trình có nghiệm kép x="),
        VoSoNghiem("phương trình vô số nghiêm"),
       VoNghiem("phương trình vô nghiệm"),
       MotNghiem("phương trình có ngiệm duy nhất x="),
       HaiNghiem("phương trình có 2 nghiệm "),
       BaNghiem("phương trình có 3 nghiệm ");
       private String msg;
      ListRootEquation(String msg)
       {
            this.msg=msg;
       }
       public String decription() {
           return this.msg;
       }

        public String getMsg() {
        return msg;
    }
}
