package com.example.equatation.model;

public class ThreeEquation {

    private int a[]=new int[3];
    private int b[]=new int[3];
    private int c[]=new int[3];
    private int d[]=new int[3];

    public ThreeEquation(int[] a, int[] b, int[] c, int[] d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }

   public String solveThreeVariable()
    {
        int d=SolveD(this.a,this.b,this.c);
        int dx=Solvedx(this.d,this.b,this.c);
        int dy=Solvedy(this.a,this.d,this.c);
        int dz=Solvedz(this.a,this.b,this.d);
        if(d==0 && dx==0 && dy==0 && dz==0)
        {
            return ListRootThreeEqua.HeVoNghiem.decription() ;
        }
        else if(d==0 && (dx==0 || dy==0 || dz==0))
        {
            return ListRootThreeEqua.HeVoNghiem.toString();
        }
        else if(d!=0)
        {
            double x=dx*1.0/d;
            double y=dy*1.0/d;
            double z=dz*1.0/d;
            String nghiem=ListRootThreeEqua.HeCoNghiem.decription()+x+"\n"+"y="+y+"\nz="+z;
            return nghiem;
        }
        return "";
    }
    private int SolveD(int []a,int[]b,int[]c)
    {
        int d=a[0]*b[1]*c[2]+b[0]*c[1]*a[2]+c[0]*a[1]*b[2]-a[2]*b[1]*c[0]-b[2]*c[1]*a[0]-c[2]*a[1]*b[0];
        return d;
    }
    private int Solvedx(int []d,int []b,int []c)
    {
        int dx=d[0]*b[1]*c[2]+b[0]*c[1]*d[2]+c[0]*d[1]*b[2]-d[2]*b[1]*c[0]-b[2]*c[1]*d[0]-c[2]*d[1]*b[0];
        return dx;
    }
    private int Solvedy(int []a,int[]d,int[]c)
    {
        int dy=a[0]*d[1]*c[2]+d[0]*c[1]*a[2]+c[0]*a[1]*d[2]-a[2]*d[1]*c[0]-d[2]*c[1]*a[0]-c[2]*a[1]*d[0];
        return dy;
    }
    private int Solvedz(int []a,int[]b,int[]d)
    {
        return a[0]*b[1]*d[2]+b[0]*d[1]*a[2]+d[0]*a[1]*b[2]-a[2]*b[1]*d[0]-b[2]*d[1]*a[0]-d[2]*a[1]*b[0];
    }

}
