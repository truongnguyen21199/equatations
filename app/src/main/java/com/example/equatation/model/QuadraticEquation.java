package com.example.equatation.model;

import android.util.Log;

public class QuadraticEquation {
    private double a;
    private double b;
    private double c;


    public String SolveEquation() {
        if (this.a == 0.0) {
            if (this.b == 0.0 && this.c == 0.0) {
                return ListRootEquation.VoSoNghiem.decription();
            } else if (this.b == 0.0 && this.c != 0.0) {
                return ListRootEquation.VoNghiem.getMsg();
            } else {
                return ListRootEquation.MotNghiem.decription() + (-this.c /this.b);
            }
        }
        double delta = this.b * this.b - 4.0 * this.a * this.c;
        double x1;
        double x2;
        if (delta > 0.0) {
            if (delta > 0.0) {
                x1 = ((-this.b + Math.sqrt(delta)) / (2 * this.a));
                x2 = ((-this.b - Math.sqrt(delta)) / (2 * this.a));
                return ListRootEquation.HaiNghiem.decription() + "x1 = " + x1 + " và x2 = " + x2;

            } else if (delta == 0.0) {
                x1 = (-this.b / (2.0 * this.a));
                return ListRootEquation.NghiemKep.decription() + x1;
            }
        } else {
            return ListRootEquation.VoNghiem.decription();
        }
        return "";
    }

    public QuadraticEquation(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public QuadraticEquation() {

    }
}
