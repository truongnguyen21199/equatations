package com.example.equatation.model;

import android.util.Log;

public class CubicEquation {
    private double a;
    private double b;
    private double c;
    private double d;

    public CubicEquation(double a, double b, double c, double d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }
    public String solveCubicEquation()
    {
        double delta=Math.pow(this.b,2)-3*this.a*this.c;
        double k=(9*this.a*this.b*this.c-2*Math.pow(this.b,3)-27*Math.pow(this.a,2)*this.d)/(2*Math.sqrt(Math.pow(Math.abs(delta),3)));
        if(delta>0)
        {
            if(Math.abs(k)<=1)
            {
                double x1= (2*Math.sqrt(delta)*Math.cos((Math.acos(k)/3))-this.b)/(3*this.a);
                double x2= (2*Math.sqrt(delta)*Math.cos((Math.acos(k)/3-2*Math.PI/3))-this.b)/(3*this.a);
                double x3= (2*Math.sqrt(delta)*Math.cos((Math.acos(k)/3+(2*Math.PI)/3))-this.b)/(3*this.a);
                return ListRootEquation.BaNghiem.decription()+x1+"\nx2="+x2+"\nx3"+x3;

            }
            else if(Math.abs(k)>1)
            {
                double x=((Math.sqrt(delta)*Math.abs(k))/(3*this.a*k)) * ((Math.cbrt(Math.abs(k)+Math.sqrt(Math.pow(k,2)-1)))+(Math.cbrt(Math.abs(k)-Math.sqrt(Math.pow(k,2)-1))))-this.b/(3*this.a);
                Log.d("x=", x+"");
                return ListRootEquation.MotNghiem.getMsg()+x;
            }
        }
        else  if(delta==0)
        {
            double x=(-this.b+Math.cbrt(Math.pow(this.b,3)-27*Math.pow(this.a,2)*this.d))/(3*this.a);
            return ListRootEquation.MotNghiem.getMsg()+x;
        }
        else if(delta<0)
        {
            double x=((Math.sqrt(Math.abs(delta)))/(3*this.a)) * ( Math.cbrt(k+Math.sqrt(Math.pow(k,2)+1)) + Math.cbrt(k-Math.sqrt(Math.pow(k,2)+1))) -this.b/(3*this.a);
           return ListRootEquation.MotNghiem.getMsg()+x;
        }
        return "";
    }

    public CubicEquation() {
    }

}
