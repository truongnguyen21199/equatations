package com.example.equatation.dialog;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.equatation.screens.ThreeVariableFrament;
import com.example.equatations.R;


public class ThreeVariableDialog extends DialogFragment {
    private EditText txtA1, txtB1, txtC1, txtD1;
    private TextView txtOk;
    private TextView txtTitleDialog;
    public ThreeDialogListener threeDialogListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.layout_dialog,container,false);
        txtOk=view.findViewById(R.id.txtOk);
        txtTitleDialog=view.findViewById(R.id.txtTitleDialog);
        txtTitleDialog.setText(ThreeVariableFrament.titleDialog);
        txtA1=view.findViewById(R.id.txtA1);
        txtB1=view.findViewById(R.id.txtB1);
        txtC1=view.findViewById(R.id.txtC1);
        txtD1=view.findViewById(R.id.txtD1);
        txtOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String a=txtA1.getText().toString();
                String b=txtB1.getText().toString();
                String c=txtC1.getText().toString();
                String d=txtD1.getText().toString();
                if(!a.equals("") && !b.equals("")&& !c.equals("") && !d.equals(""))
                {
                    threeDialogListener.setTextDialogA(a,b,c,d);
                }
                txtA1.setText("");
                txtB1.setText("");
                txtC1.setText("");
                txtD1.setText("");
                getDialog().dismiss();
            }
        });
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            threeDialogListener = (ThreeDialogListener)  getTargetFragment();
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement listeners!");
        }

    }
    public interface ThreeDialogListener {
        void setTextDialogA(String a, String b,String c,String d);

    }
}
