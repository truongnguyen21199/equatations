package com.example.equatation.screens;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.equatation.MainActivity;
import com.example.equatation.model.CubicEquation;
import com.example.equatation.model.QuadraticEquation;
import com.example.equatations.R;

import java.util.concurrent.TimeUnit;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.CompletableEmitter;
import io.reactivex.rxjava3.core.CompletableObserver;
import io.reactivex.rxjava3.core.CompletableOnSubscribe;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.schedulers.Schedulers;
import io.reactivex.rxjava3.subscribers.DefaultSubscriber;


public class CubicQuaraFrament extends Fragment implements View.OnClickListener{
    private EditText txtA, txtB, txtC, txtD;
    private TextView textViewD;
    private TextView txtResult;
    private Button btnSolve;
    double a=-1;
    double b=-1;
    double c=-1;



    double d=-1;
    double x1,x2,x3;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return  inflater.inflate(R.layout.fragment_title1, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        txtA = view.findViewById(R.id.txtA);
        txtB = view.findViewById(R.id.txtB);
        txtC = view.findViewById(R.id.txtC);
        txtD = view.findViewById(R.id.txtD);
        btnSolve=view.findViewById(R.id.btnSlove);
        btnSolve.setOnClickListener(this);
        txtResult = view.findViewById(R.id.txtResult);
        textViewD = view.findViewById(R.id.textViewD);
        if (HomeEquationFrament.mark) {
            textViewD.setVisibility(View.GONE);
            txtD.setVisibility(View.GONE);
        }

        /*Toolbar toolbar=  view.findViewById(R.id.toolbar1);
        if (toolbar != null) {
            ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        }
        toolbar.setTitle("Quadratic Equation");*/

        if(!HomeEquationFrament.mark)
        {
            textViewD.setVisibility(View.VISIBLE);
            txtD.setVisibility(View.VISIBLE);
        }
        super.onViewCreated(view, savedInstanceState);
    }

    private void processSlove(double a, double b, double c) {

        //Subscriber: bộ đăng kí phát tín hiệu,input
        //Observable: bộ phát  Publisher
        //Observer: bộ nhận tín hiệu từ máy phát,outpush
        //Cusumer:(bộ tiêu thụ) nơi lưu trữ dữ liệu trả về
        //Emiter:tín hiệu phát ra
        //Disposible: nơi lưu trữ luồng xử lý
        // Operator

        // du lieu dau do chay vao Subcriber ->Observable->Observer



        /*Observable<QuadraticEquation>observable;
        observable=Observable.just(new QuadraticEquation(a,b,c));//input đi vào bộ dăng kí toan tử just
         //Long running task
        observable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                /<một số toán tử xử lý data>sẽ ở đây>
                .subscribe(new Observer<QuadraticEquation>() {//
                    @Override
                    public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@io.reactivex.rxjava3.annotations.NonNull QuadraticEquation quadraticEquation) {
                            //Update the progress to UI using data from ProgressInfo
                    }

                    @Override
                    public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                          //Task is completed with a Result
                    }
                });*/

        //============================================================



            /*Observable<QuadraticEquation>observable;
            observable=Observable.create(new ObservableOnSubscribe<QuadraticEquation>() {//custom bộ đăng kí//thay cho toán tử just
                @Override
                public void subscribe(@io.reactivex.rxjava3.annotations.NonNull ObservableEmitter<QuadraticEquation> emitter) throws Throwable {
                    emitter.onNext(new QuadraticEquation(a,b,c)); //to onNext
                    emitter.onComplete();//.....// input đầu vào đi vào bộ dăng kí
                    throw new Exception(); //.....//Long running task
                }
            });
            /*observable.subscribe(new Observer<QuadraticEquation>() {
                @Override
                public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {

                }

                @Override
                public void onNext(@io.reactivex.rxjava3.annotations.NonNull QuadraticEquation quadraticEquation) {

                }

                @Override
                public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {

                }

                @Override
                public void onComplete() {

                }
            });*/
            /*observable.subscribe(new Consumer<QuadraticEquation>() {//chỉ quan tâm kết quả tra về
                @Override
                public void accept(QuadraticEquation quadraticEquation) throws Throwable {

                }
            });
        //=================================================================
            */
        //chia làm đôi
      /* Observable.just(new QuadraticEquation(a,b,c)).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
               .map(new Function<QuadraticEquation, String>() {
           //map data sang String
            //subcriberOn đinh nghĩa lịch trình Subscriber  chạy ở đâu input //đi theo tưng cặp
            //các loại thread Shedule,AndroidC,newthread......
            //  observeOn đinh nghia lich trinh Observer output
           @Override
           public String apply(QuadraticEquation quadraticEquation) throws Throwable {
               return quadraticEquation.toString();//conver dư liệu
           }
       }).subscribe(new Consumer<String>() {//observer nhận dư liệu
           @Override
           public void accept(String s) throws Throwable {
                //dư lieu tra ve
           }
       });*/
        //=======================================================================================================
         //Observable<Integer>observable=Observable.range(1,5000).sample(1, TimeUnit.MICROSECONDS);
        // =========================================================================================================
        Observable.just(new QuadraticEquation(a,b,c)).subscribeOn(Schedulers.io())
                .map(new Function<QuadraticEquation, String>() {
                    //map data sang String
                    //subcriberOn đinh nghĩa lịch trình Subscriber  chạy ở đâu input //đi theo tưng cặp
                    //các loại thread Shedule.io,AndroidC...,newthread......
                    //  observeOn đinh nghia lich trinh Observer output
                    @Override
                    public String apply(QuadraticEquation quadraticEquation) throws Throwable {
                        return quadraticEquation.SolveEquation();//convert dư liệu
                    }
                }).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<String>() {//observer nhận dư liệu
            @Override
            public void accept(String s) throws Throwable {
                txtResult.setText(s);
            }
        });
        //=========================================================================================================
        //iput chi một data duy nhất
       /* Single.just(new QuadraticEquation()).subscribe(new SingleObserver<QuadraticEquation>() {
            @Override
            public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {

            }

            @Override
            public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull QuadraticEquation quadraticEquation) {

            }

            @Override
            public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {

            }
        });*/
        //==========================================================================================================
        //no care input data
       /* Completable.create(new CompletableOnSubscribe() {//vi du sau 5s kich lần
            @Override
            public void subscribe(@io.reactivex.rxjava3.annotations.NonNull CompletableEmitter emitter) throws Throwable {

            }
        }).delay(5,TimeUnit.MICROSECONDS).subscribe(new CompletableObserver() {
            @Override
            public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {

            }

            @Override
            public void onComplete() {

            }

            @Override
            public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {

            }
        });*/
        //============================================================================================================
       /* Flowable<Integer> flowable=Flowable.range(1,5000).sample(1,TimeUnit.MICROSECONDS);
        flowable.subscribe(new DefaultSubscriber<Integer>() {
            @Override
            public void onNext(Integer integer) {

            }

            @Override
            public void onError(Throwable t) {

            }

            @Override
            public void onComplete() {

            }
        });*/
        //map flatmap,merg,concat
    }

    @Override
    public void onClick(View view) {
        if(view.getId()==R.id.btnSlove && HomeEquationFrament.mark)
        {
            if(txtA.getText().toString().equals("")||
            txtB.getText().toString().equals("") || txtC.getText().toString().equals(""))
            {
                return;
            }
             else {
                     a = Double.parseDouble(txtA.getText().toString());
                     b = Double.parseDouble(txtB.getText().toString());
                     c = Double.parseDouble(txtC.getText().toString());
                     processSlove(a, b, c);
                     Log.d("Loi a",txtA.getText().toString());
            }
            txtA.setText("");
            txtB.setText("");
            txtC.setText("");
            txtA.requestFocus();
            //hide keyboard
            View view1 = this.getActivity().getCurrentFocus();
            if (view1 != null) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view1.getWindowToken(), 0);
            }

        }
        if (view.getId()==R.id.btnSlove && !HomeEquationFrament.mark)
        {
            if(txtA.getText().toString().equals("")||
                    txtB.getText().toString().equals("")
                    || txtC.getText().toString().equals("") || txtD.getText().toString().equals("")) {
                return;
            }
            a = Double.parseDouble(txtA.getText().toString());
            b = Double.parseDouble(txtB.getText().toString());
            c = Double.parseDouble(txtC.getText().toString());
            d=Double.parseDouble(txtD.getText().toString());
            Log.d("abcd",(a+b+c+d+"") );
            processSloveCubic(a, b, c,d);
        }
        txtA.setText("");
        txtB.setText("");
        txtC.setText("");
        txtD.setText("");
        txtA.requestFocus();
        //hide keyboard
        View view1 = this.getActivity().getCurrentFocus();
        if (view1 != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view1.getWindowToken(), 0);
        }

    }

    private void processSloveCubic(double a1, double b1, double c1, double d1) {
        Observable.just(new CubicEquation(a,b,c,d)).subscribeOn(Schedulers.io())
                .map(new Function<CubicEquation, String>() {
                    //map data sang String
                    //subcriberOn đinh nghĩa lịch trình Subscriber  chạy ở đâu input //đi theo tưng cặp
                    //các loại thread Shedule.io,AndroidC...,newthread......
                    //  observeOn đinh nghia lich trinh Observer output
                    @Override
                    public String apply(CubicEquation cubicEquation) throws Throwable {
                        return cubicEquation.solveCubicEquation();//convert dư liệu
                    }
                }).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<String>() {//observer nhận dư liệu
            @Override
            public void accept(String s) throws Throwable {
                txtResult.setText(s);
            }
        });
    }
}