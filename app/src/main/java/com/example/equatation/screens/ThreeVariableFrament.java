package com.example.equatation.screens;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;


import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.equatation.dialog.ThreeVariableDialog;
import com.example.equatation.model.QuadraticEquation;
import com.example.equatation.model.ThreeEquation;
import com.example.equatations.R;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.schedulers.Schedulers;


public class ThreeVariableFrament extends Fragment implements ThreeVariableDialog.ThreeDialogListener{
    private ImageButton btnEquatation1;
    private ImageButton btnEquatation2;
    private ImageButton btnEquatation3;
    private Button btnSolveThree;
    private TextView txtResultThree;
    boolean checkbutton=false;
    boolean checkbutton1=false;
    boolean checkbutton2=false;

    int a1[]=new int[3];
    int b1[]=new int[3];
    int c1[]=new int[3];
    int d1[]=new int[3];
     TextView txtEQ1,txtEQ2,txtEQ3;
     ImageButton btnLasteadselected=null;
   public static String titleDialog="";
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_title2, container, false);
        btnEquatation1=view.findViewById(R.id.btnEquatation1);
        btnEquatation2=view.findViewById(R.id.btnEquatation2);
        btnEquatation3=view.findViewById(R.id.btnEquatation3);
        txtEQ1=view.findViewById(R.id.txtEquatation1);
        txtEQ2=view.findViewById(R.id.txtEquatation2);
        txtResultThree=view.findViewById(R.id.txtResultThree);
        txtEQ3=view.findViewById(R.id.txtEquatation3);
        btnSolveThree=view.findViewById(R.id.btnSloveThree);
        btnEquatation1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnLasteadselected=btnEquatation1;
                titleDialog="Equatition 1";
                checkbutton=true;
                checkbutton1=false;
                checkbutton2=false;
                openDialog();
            }
        });
        btnEquatation2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnLasteadselected=btnEquatation2;
                titleDialog="Equatition 2";
                checkbutton=false;
                checkbutton1=true;
                checkbutton2=false;
                openDialog();
            }
        });
        btnEquatation3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnLasteadselected=btnEquatation3;
                titleDialog="Equatition 3";
                checkbutton=false;
                checkbutton1=false;
                checkbutton2=true;
                openDialog();
            }
        });
        btnSolveThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                processThreeVariable();
            }
        });
        return view;
    }

    private void processThreeVariable() {
        /*a1,b1,c1,a1,b1
          a2,b2,c2,a2,b2
          a3,b3,c3,a3,b3*/
        /*
        d1 b1 c1 d1 b1
        d2 b2 c2 d2 b2
        d3 b3 c3 d3 b3
         */
        /*
        a1 d1 c1 a1 d1
        a2 d2 c2 a2 d2
        a3 d3 c3 a3 d3
         */
        /*
        a1 b1 d1 a1 b1
        a2 b2 d2 a2 b2
        a3 b3 d3 a3 b3
         */

        //Subscriber: bộ đăng kí phát tín hiệu:
        //Observable: bộ phát  Publisher
        //Observer: bộ nhận tín hiệu từ máy phát
        //Cusumer:(bộ tiêu thụ) nơi lưu trữ dữ liệu trả về
        //Emiter:tín hiệu phát ra
        //Disposible: nơi lưu trữ luồng xử lý
        // Operator

        // du lieu dau do chay vao Subcriber ->Observable->Observer



        /*Observable<QuadraticEquation>observable;
        observable=Observable.just(new QuadraticEquation(a,b,c));//input đi vào bộ dăng kí toan tử just
         //Long running task
        observable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                /<một số toán tử xử lý data>sẽ ở đây>
                .subscribe(new Observer<QuadraticEquation>() {//
                    @Override
                    public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@io.reactivex.rxjava3.annotations.NonNull QuadraticEquation quadraticEquation) {
                            //Update the progress to UI using data from ProgressInfo
                    }

                    @Override
                    public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                          //Task is completed with a Result
                    }
                });*/

        //============================================================



            /*Observable<QuadraticEquation>observable;
            observable=Observable.create(new ObservableOnSubscribe<QuadraticEquation>() {//custom bộ đăng kí//thay cho toán tử just
                @Override
                public void subscribe(@io.reactivex.rxjava3.annotations.NonNull ObservableEmitter<QuadraticEquation> emitter) throws Throwable {
                    emitter.onNext(new QuadraticEquation(a,b,c)); //to onNext
                    emitter.onComplete();//.....// input đầu vào đi vào bộ dăng kí
                    throw new Exception(); //.....//Long running task
                }
            });
            /*observable.subscribe(new Observer<QuadraticEquation>() {
                @Override
                public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {

                }

                @Override
                public void onNext(@io.reactivex.rxjava3.annotations.NonNull QuadraticEquation quadraticEquation) {

                }

                @Override
                public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {

                }

                @Override
                public void onComplete() {

                }
            });*/
            /*observable.subscribe(new Consumer<QuadraticEquation>() {//chỉ quan tâm kết quả tra về
                @Override
                public void accept(QuadraticEquation quadraticEquation) throws Throwable {

                }
            });
        //=================================================================
            */
        //chia làm đôi
      /* Observable.just(new QuadraticEquation(a,b,c)).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
               .map(new Function<QuadraticEquation, String>() {
           //map data sang String
            //subcriberOn đinh nghĩa lịch trình Subscriber  chạy ở đâu input //đi theo tưng cặp
            //các loại thread Shedule,AndroidC,newthread......
            //  observeOn đinh nghia lich trinh Observer output
           @Override
           public String apply(QuadraticEquation quadraticEquation) throws Throwable {
               return quadraticEquation.toString();//conver dư liệu
           }
       }).subscribe(new Consumer<String>() {//observer nhận dư liệu
           @Override
           public void accept(String s) throws Throwable {
                //dư lieu tra ve
           }
       });*/
        //=======================================================================================================
        //Observable<Integer>observable=Observable.range(1,5000).sample(1, TimeUnit.MICROSECONDS);
        // =========================================================================================================
        Observable.just(new ThreeEquation(a1,b1,c1,d1)).subscribeOn(Schedulers.io())
                .map(new Function<ThreeEquation, String>() {
                    //map data sang String
                    //subcriberOn đinh nghĩa lịch trình Subscriber  chạy ở đâu input //đi theo tưng cặp
                    //các loại thread Shedule.io,AndroidC...,newthread......
                    //  observeOn đinh nghia lich trinh Observer output
                    @Override
                    public String apply(ThreeEquation threeEquation) throws Throwable {
                        return threeEquation.solveThreeVariable();//convert dư liệu
                    }
                }).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<String>() {//observer nhận dư liệu
            @Override
            public void accept(String s) throws Throwable {
                txtResultThree.setText(s);
            }
        });
        //=========================================================================================================
        //iput chi một data duy nhất
       /* Single.just(new QuadraticEquation()).subscribe(new SingleObserver<QuadraticEquation>() {
            @Override
            public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {

            }

            @Override
            public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull QuadraticEquation quadraticEquation) {

            }

            @Override
            public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {

            }
        });*/
        //==========================================================================================================
        //no care input data
       /* Completable.create(new CompletableOnSubscribe() {//vi du sau 5s kich lần
            @Override
            public void subscribe(@io.reactivex.rxjava3.annotations.NonNull CompletableEmitter emitter) throws Throwable {

            }
        }).delay(5,TimeUnit.MICROSECONDS).subscribe(new CompletableObserver() {
            @Override
            public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {

            }

            @Override
            public void onComplete() {

            }

            @Override
            public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {

            }
        });*/
        //============================================================================================================
       /* Flowable<Integer> flowable=Flowable.range(1,5000).sample(1,TimeUnit.MICROSECONDS);
        flowable.subscribe(new DefaultSubscriber<Integer>() {
            @Override
            public void onNext(Integer integer) {

            }

            @Override
            public void onError(Throwable t) {

            }

            @Override
            public void onComplete() {

            }
        });*/
        //map flatmap,merg,concat

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void openDialog() {
        ThreeVariableDialog dialog = new ThreeVariableDialog();
        dialog.setTargetFragment(ThreeVariableFrament.this,1);
        dialog.show(getFragmentManager(),"ThreeVariableDialog");

    }


    @Override
    public void setTextDialogA(String a, String b, String c, String d) {
        if(checkbutton==true) {
            txtEQ1.setText((new StringBuilder()).append(a + "x+").append(b + "y+").append(c + "z=").append(d).toString());
            a1[0]=Integer.parseInt(a);
            b1[0]=Integer.parseInt(b);
            c1[0]=Integer.parseInt(c);
            d1[0]=Integer.parseInt(d);

        }
        else if(checkbutton1==true)
        {
            a1[1]=Integer.parseInt(a);
            b1[1]=Integer.parseInt(b);
            c1[1]=Integer.parseInt(c);
            d1[1]=Integer.parseInt(d);
            txtEQ2.setText((new StringBuilder()).append(a+"x+").append(b+"y+").append(c+"z=").append(d).toString());
        }
        else if (checkbutton2==true)
        {
            a1[2]=Integer.parseInt(a);
            b1[2]=Integer.parseInt(b);
            c1[2]=Integer.parseInt(c);
            d1[2]=Integer.parseInt(d);
            txtEQ3.setText((new StringBuilder()).append(a+"x+").append(b+"y+").append(c+"z=").append(d).toString());

        }

    }
    public interface setTitleDilogListener
    {
        void setTitledilog(String titledilog);
    }

}