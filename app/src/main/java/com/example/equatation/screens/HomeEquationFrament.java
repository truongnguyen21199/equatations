package com.example.equatation.screens;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.equatations.R;


public class HomeEquationFrament extends Fragment {
    public static boolean mark=true;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_main_title, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final NavController navController= Navigation.findNavController(view);
        Button btnQuaDraticEquation=view.findViewById(R.id.btnQuaDraticEquation);
       Button btnCubicEquation= view.findViewById(R.id.btnCubicEquation);
       Button btnThreeVariables=view.findViewById(R.id.btnThreeVariables);
       btnQuaDraticEquation.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               mark=true;
                navController.navigate(R.id.action_mainTitle_to_title1);
           }
       });
       btnCubicEquation.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               mark=false;
               navController.navigate(R.id.action_mainTitle_to_title1);
           }
       });
       btnThreeVariables.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               navController.navigate(R.id.action_mainTitle_to_title2);
           }
       });
    }
}